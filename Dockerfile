FROM debian:jessie

# These dependencies aren't necessary for running Go per se, but are
#  useful to have around
RUN export DEBIAN_FRONTEND='noninteractive' && \
    apt-get update  && \
    apt-get install -y screen mercurial git wget rsync net-tools

